import string
import random

def init_jeu(nb_lignes, nb_colonnes):
    # LE PLATEAU
    global cols
    cols = nb_colonnes
    global rows
    rows = nb_lignes
    plateau = []
    for _ in range(nb_lignes):
        plateau.append(["."]*rows)
    
    # LE ROI
    c_roi = nb_colonnes // 2
    l_roi = nb_lignes // 2
    
    # LA PIOCHE
    directions = ["N", "NE", "E", "SE", "S", "SO", "O", "NO"]
    nombres = ["1", "2", "3"]
    pioche = []
    n = 0
    while n != len(directions):
        k = 0
        while k != len(nombres): 
            pioche.append(directions[n] + nombres[k])
            k += 1
        n += 1
    random.shuffle(pioche)

    # LA MAIN DES JOUEURS
    # main du joueur rouge
    main_r = random.sample(pioche,5)
    pioche = set(pioche) - set(main_r)
    setp = set(pioche)
    setmr = set(main_r)
    pioche = list(setp - setmr)

    # main du joueur blanc
    main_b = random.sample(pioche,5)
    pioche = set(pioche) - set(main_b)
    setp = set(pioche)
    setmb = set(main_b)
    pioche = list(setp - setmb)

    # LA DEFAUSSE
    defausse = []
    return (plateau, l_roi, c_roi, main_r, main_b, pioche, defausse)

def mouvement_possible(plateau , l_roi , c_roi , carte):
    direction = carte.rstrip(string.digits)
    combien = carte.lstrip(string.ascii_letters)
    combien = int(combien)
    l_roi_possible = l_roi
    c_roi_possible = c_roi
    if direction == "N":    
        l_roi_possible -= combien
    elif direction == "NE":
        l_roi_possible -= combien
        c_roi_possible += combien
    elif direction == "NO":
        l_roi_possible -= combien
        c_roi_possible -= combien
    elif direction == "S":
        l_roi_possible += combien
    elif direction == "SE":
        l_roi_possible += combien
        c_roi_possible += combien
    elif direction == "SO":
        l_roi_possible += combien
        c_roi_possible -= combien                
    elif direction == "O":    
        c_roi_possible -= combien
    elif direction == "E":
        c_roi_possible += combien
    else:
        raise ValueError()
    if (l_roi_possible < 0 or l_roi_possible > 8 or c_roi_possible < 0 or c_roi_possible > 8):
        return False
    elif (plateau[l_roi_possible][c_roi_possible] != "."):
        return False
    else: 
        return True

def bouge_le_roi(plateau, l_roi, c_roi, main_r, main_b, defausse , carte , couleur):
    direction = carte.rstrip(string.digits)
    combien = carte.lstrip(string.ascii_letters)
    combien = int(combien)
    if direction == "N":    
        l_roi -= combien
    elif direction == "NE":
        l_roi -= combien
        c_roi += combien
    elif direction == "NO":
        l_roi -= combien
        c_roi -= combien
    elif direction == "S":
        l_roi += combien
    elif direction == "SE":
        l_roi += combien
        c_roi += combien
    elif direction == "SO":
        l_roi += combien
        c_roi -= combien                
    elif direction == "O":    
        c_roi -= combien
    elif direction == "E":
        c_roi += combien
    else:
        raise ValueError()
    if couleur == "Rouge":
        plateau[l_roi][c_roi] = "R"
        main_r.remove(carte)
    elif couleur == "Blanc":
        plateau[l_roi][c_roi] = "B"
        main_b.remove(carte)
    defausse.append(carte)
    return (plateau, main_r, main_b, defausse, l_roi, c_roi, carte)

def name():
    # c'est le nom de mon beau gosse de chat : 0 originalité
    return "Billy"

def play(plateau , l_roi , c_roi , main_r , main_b , couleur):
    global carte_ia
    main = main_b
    pion = couleur[0]
    if pion == "R":
        pion_adverse = "B"
    else:
        pion_adverse = "R"
    if couleur == "Rouge":
        main = main_r
    #if len(main) < 5: 
    #    return "pioche"
    #else:
    for carte_ia in main:
        if mouvement_possible(plateau , l_roi , c_roi , carte = carte_ia):
            # au premier tour
            if pion not in plateau:
            # on va anticiper la position du roi
            # si le roi est dans la zone nord du plateau
                if l_roi < 4:
                    # si le roi est dans la zone NO du plateau
                    if c_roi < 4 :
                        # on retourne au milieu du plateau si l'on dispose d'une de ces cartes
                        # meilleur choix
                        if carte_ia == "SE1" or "SE2" or "SE3":
                            return carte_ia
                        # second choix
                        elif carte_ia == "O1" or "O2" or "O3" or "S1" or "S2" or "S3":
                            return carte_ia
                        # si aucune de ces cartes, on laisse le hasard décider, pas le choix 
                    # si le roi est dans la zone NE du plateau
                    elif c_roi > 4 :
                        if carte_ia == "SO1" or "SO2" or "SO3":
                            return carte_ia
                        elif carte_ia == "E1" or "E2" or "E3" or "S1" or "S2" or "S3":
                            return carte_ia
                    # si le roi se trouve sur la moitié nord de la colonne du milieu du plateau (c_roi == 4)
                    else :
                        if carte_ia == "S1" or "S2" or "S3":
                            return carte_ia
                        elif carte_ia == "SE1" or "SE2" or "SE3" or "SO1" or "SO2" or "SO3":
                            return carte_ia
                # si le roi est dans la zone sud du plateau
                elif l_roi > 4:
                    # si le roi est dans la zone SO du plateau
                    if c_roi < 4 :
                        if carte_ia == "NE1" or "NE2" or "NE3":
                            return carte_ia
                        elif carte_ia == "O1" or "O2" or "O3" or "N1" or "N2" or "N3":
                            return carte_ia
                    # si le roi est dans la zone SE du plateau
                    elif c_roi > 4 :
                        if carte_ia == "NO1" or "NO2" or "NO3":
                            return carte_ia
                        elif carte_ia == "E1" or "E2" or "E3" or "S1" or "S2" or "S3":
                            return carte_ia
                    # si le roi se trouve sur la moitié sud de la colonne du milieu du plateau (c_roi == 4)   
                    else :
                        # premier choix
                        if carte_ia == "N1" or "N2" or "N3":
                            return carte_ia
                        # second choix si on a pas les cartes du dessus dans notre main
                        elif carte_ia == "NE1" or "NE2" or "NE3" or "NO1" or "NO2" or "NO3":
                            return carte_ia
                # si le roi se trouve sur ligne du milieu du plateau (l_roi == 4)
                else :
                    # si le roi se situe dans la partie ouest de la ligne du milieu
                    if c_roi < 4 :
                        # premier choix
                        if carte_ia == "E1" or "E2" or "E3":
                            return carte_ia
                        # deuxieme choix
                        elif carte_ia == "SE1" or "SE2" or "SE3" or "NE1" or "NE2" or "NE3":
                            return carte_ia
                    elif c_roi > 4 :
                        if carte_ia == "O1" or "O2" or "O3":
                            return carte_ia
                    # le roi se situe pile au milieu du plateau : c'est à l'IA de commencer 
                    # on décide de déplacer le roi d'une case au mieux si on dispose d'une de ces cartes ci-dessous (8 chances/24 soit 1 chance/3 d'avoir la carte)
                    # cela permet de commencer notre zone au milieu du plateau pour l'étendre ensuite
                    else:
                        if carte_ia == "O1" or "E1" or "S1" or "N1" or "NE1" or "NO1" or "SE1" or "SO1":
                            return carte_ia
            else:   
            # si dans un périmètre de 3x3 cases autour du roi il y a un pion de notre couleur, on essaye de s'en rapprocher (au mieux juste à côté)
            # # on definit les limites de notre perimetre
                l_roiperi_min = l_roi - 3
                l_roiperi_max = l_roi + 3
                c_roiperi_min = c_roi - 3
                c_roiperi_max = c_roi + 3
                if l_roiperi_max > 8 :
                    l_roiperi_max = 8
                if l_roiperi_min < 0:
                    l_roiperi_min = 0
                if c_roiperi_max > 8 :
                    c_roiperi_max = 8
                if c_roiperi_min < 0:
                    c_roiperi_min = 0
                perimetre_roi = []       
                for i in range(0, 9):
                    perimetre_roi.append([])
                    for j in range(c_roiperi_min,c_roiperi_max+1):
                        if c_roiperi_min <= j <= c_roiperi_max:
                            perimetre_roi[i].append(plateau[i][j])
                perimetre_roi = perimetre_roi[l_roiperi_min:l_roiperi_max+1]
                if pion in perimetre_roi:
                    for i in range(0,len(perimetre_roi)):
                        for j in range(0,len(perimetre_roi)):
                            if perimetre_roi[i][j] == pion:
                                if bouge_le_roi(plateau, l_roi, main_r, main_b, defausse, carte_ia, couleur) == (plateau, i, j-1, main_r, main_b, defausse):
                                    return carte_ia
                                elif bouge_le_roi(plateau, l_roi, main_r, main_b, defausse, carte_ia, couleur) == (plateau, i, j+1, main_r, main_b, defausse):
                                    return carte_ia
                                elif bouge_le_roi(plateau, l_roi, main_r, main_b, defausse, carte_ia, couleur) == (plateau, i+1, j, main_r, main_b, defausse):
                                    return carte_ia
                                elif bouge_le_roi(plateau, l_roi, main_r, main_b, defausse, carte_ia, couleur) == (plateau, i-1, j, main_r, main_b, defausse):
                                    return carte_ia
                                elif len(main) < 5: 
                                    return "pioche"
                                else:
                                    return carte_ia
                else:
                    # on essaye en premier lieu d'empêcher le joueur adverse d'agrandir son eventuel zone 
                    # comme c'est le cas au morpion ou au puissance 4, on place un de nos pions à l'interstice de deux de ses pions pour minimiser sa zone à lui
                    if pion_adverse in perimetre_roi:
                        for i in range(0,len(perimetre_roi)):
                            for j in range(0,len(perimetre_roi)):
                                if perimetre_roi[i][j] == pion_adverse:
                                    # si il y a un pion adverse à deux cases à gauche de ce pion
                                    if perimetre_roi[i][j-2] == pion_adverse:
                                        if bouge_le_roi(plateau, l_roi, main_r, main_b, defausse, carte_ia, couleur) == (plateau, i, j-1, main_r, main_b, defausse):
                                            return carte_ia
                                    # si il y a un pion adverse à deux cases à droite de ce pion
                                    elif perimetre_roi[i][j+2] == pion_adverse:
                                            if bouge_le_roi(plateau, l_roi, main_r, main_b, defausse, carte_ia, couleur) == (plateau, i, j+1, main_r, main_b, defausse):
                                                return carte_ia
                                    # si il y a un pion adverse à deux cases en haut de ce pion
                                    elif perimetre_roi[i-2][j] == pion_adverse:
                                        if bouge_le_roi(plateau, l_roi, main_r, main_b, defausse, carte_ia, couleur) == (plateau, i-1, j, main_r, main_b, defausse):
                                            return carte_ia
                                    # si il y a un pion adverse à deux cases en bas de ce pion
                                    elif perimetre_roi[i+2][j] == pion_adverse:
                                        if bouge_le_roi(plateau, l_roi, main_r, main_b, defausse, carte_ia, couleur) == (plateau, i+1, j, main_r, main_b, defausse):
                                            return carte_ia
                                    else:
                                        if len(main) < 5: 
                                            return "pioche"
                    # on joue une carte dans la direction la plus éloignée des pions du joueur adverse pour que ça soit plus compliqué pour le joueur adverse ensuite
                    else:
                        for i in range(0,len(plateau)):
                            for j in range(0,len(plateau)):
                                if plateau[i][j] == pion_adverse:
                                    if carte_ia == "SE3" or "S3" or "O3" or "E3" or "N3" or "NO3":
                                       return carte_ia
    if len(main) < 5: 
        return "pioche"
    else:
        return "passe"