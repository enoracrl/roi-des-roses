# MODELISATION
import random
import string

def init_jeu(nb_lignes, nb_colonnes):
    # LE PLATEAU
    global cols
    cols = nb_colonnes
    global rows
    rows = nb_lignes
    plateau = []
    for _ in range(nb_lignes):
        plateau.append(["."]*rows)
    
    # LE ROI
    c_roi = nb_colonnes // 2
    l_roi = nb_lignes // 2
    
    # LA PIOCHE
    directions = ["N", "NE", "E", "SE", "S", "SO", "O", "NO"]
    nombres = ["1", "2", "3"]
    pioche = []
    n = 0
    while n != len(directions):
        k = 0
        while k != len(nombres): 
            pioche.append(directions[n] + nombres[k])
            k += 1
        n += 1
    random.shuffle(pioche)

    # LA MAIN DES JOUEURS
    # main du joueur rouge
    main_r = random.sample(pioche,5)
    pioche = set(pioche) - set(main_r)
    setp = set(pioche)
    setmr = set(main_r)
    pioche = list(setp - setmr)

    # main du joueur blanc
    main_b = random.sample(pioche,5)
    pioche = set(pioche) - set(main_b)
    setp = set(pioche)
    setmb = set(main_b)
    pioche = list(setp - setmb)

    # LA DEFAUSSE
    defausse = []
    return (plateau, l_roi, c_roi, main_r, main_b, pioche, defausse)

# AFFICHAGE DE LA MAIN D'UN JOUEUR
def afficher_main(couleur , main):
    if len(main) == 5:
        print(couleur, ":", main[0], main[1], main[2], main[3], main[4])    
    elif len(main) == 4:
        print(couleur, ":", main[0], main[1], main[2], main[3])
    elif len(main) == 3:
        print(couleur, ":", main[0], main[1], main[2])
    elif len(main) == 2:
        print(couleur, ":", main[0], main[1])
    elif len(main) == 1:
        print(couleur, ":", main[0])
    else:
        print(couleur, ":")
    return main

# AFFICHAGE DU PLATEAU DE JEU 
def afficher_jeu(plateau , l_roi , c_roi , main_r , main_b):
    afficher_main("Rouge", main_r)
    afficher_main("Blanc", main_b)
    ligne = "-" * 5 * cols
    global roi, cases
    case_vide = "|" + " " + " " + " " 
    pion_rouge = "|" + " " + " " + "R"
    pion_blanc = "|" + " " + " " + "B"
    roi = "|" + " " + "X" + " "
    pion_rouge_et_roi = "|" + " " + "X" + "R"
    pion_blanc_et_roi = "|" + " " + "X" + "B"
    for l in range(0, rows):
        print(ligne)
        cases = []
        for c in range(0, cols):
            if l == l_roi and c == c_roi:
                if plateau[l][c] == "R":
                    cases.append(pion_rouge_et_roi)
                elif plateau[l][c] == "B":
                    cases.append(pion_blanc_et_roi)
                else:
                    cases.append(roi)
            else:
                if plateau[l][c] == "R":
                    cases.append(pion_rouge)
                elif plateau[l][c] == "B":                
                    cases.append(pion_blanc)
                elif plateau[l][c] == ".":
                    cases.append(case_vide)       
            cases2 = str(cases)
            cases2 = cases2.replace("'", "")
            cases2 = cases2.replace(",", "")
            cases2 = cases2.replace("[", "")
            cases2 = cases2.replace("]", "")
        print(cases2, sep = "", end = " |\n")
    print(ligne)

# DÉFINITION DES MOUVEMENTS POSSIBLES DU ROI
def mouvement_possible(plateau , l_roi , c_roi , carte):
    direction = carte.rstrip(string.digits)
    combien = carte.lstrip(string.ascii_letters)
    combien = int(combien)
    l_roi_possible = l_roi
    c_roi_possible = c_roi
    if direction == "N":    
        l_roi_possible -= combien
    elif direction == "NE":
        l_roi_possible -= combien
        c_roi_possible += combien
    elif direction == "NO":
        l_roi_possible -= combien
        c_roi_possible -= combien
    elif direction == "S":
        l_roi_possible += combien
    elif direction == "SE":
        l_roi_possible += combien
        c_roi_possible += combien
    elif direction == "SO":
        l_roi_possible += combien
        c_roi_possible -= combien                
    elif direction == "O":    
        c_roi_possible -= combien
    elif direction == "E":
        c_roi_possible += combien
    else:
        raise ValueError()
    if (l_roi_possible < 0 or l_roi_possible > 8 or c_roi_possible < 0 or c_roi_possible > 8):
        return False
    elif (plateau[l_roi_possible][c_roi_possible] != "."):
        return False 
    else:
        return True

# DÉFINTION DES CARTES JOUABLES DANS LA MAIN DU JOUEUR    
def main_jouable(plateau , l_roi , c_roi , main):
    global cartes_jouables
    cartes_jouables = []
    for carte in main:
        if mouvement_possible(plateau , l_roi , c_roi , carte) == True:
            cartes_jouables.append(carte)
    if cartes_jouables == []:
        return []
    return cartes_jouables

# DEMANDER AU JOUEUR L'ACTION À EFFECTUER
def demande_action(couleur , plateau , l_roi , c_roi , main):
    global choix, reponse, carte
    print("Vous etes le joueur", couleur, ", que souhaitez-vous faire ? ")
    choix = "KO"
    while choix != "OK":
        reponse = input()
        if reponse == "pioche":
            if len(main) >= 5:
                print("Action impossible, que souhaitez-vous faire ? ")
                choix = "KO"
            else :
                choix = "OK"
        elif reponse == "passe":
            if len(main) < 5:
                print("Action impossible, que souhaitez-vous faire ? ")
                choix = "KO"
            elif cartes_jouables != []:
                print("Action impossible, que souhaitez-vous faire ? ")
                choix = "KO"   
            else:
                choix = "OK"
        elif reponse in main :
            if len(main) == 0:
                print("Action impossible, que souhaitez-vous faire ? ")
                choix = "KO"
            elif cartes_jouables == []:
                print("Action impossible, que souhaitez-vous faire ? ")
                choix = "KO"
            else:
                carte = reponse
                if mouvement_possible(plateau , l_roi , c_roi , carte) == False:
                    print("Action impossible, que souhaitez-vous faire ? ")
                    choix = "KO"
                else:
                    if carte not in cartes_jouables:
                        print("Action impossible, que souhaitez-vous faire ? ")
                        choix = "KO"
                    else:
                        choix = "OK"
                        return carte
        else:
            print("Action impossible, que souhaitez-vous faire ? ")
    return reponse, choix

def bouge_le_roi(plateau, l_roi, c_roi, main_r, main_b, defausse , carte , couleur):
    direction = carte.rstrip(string.digits)
    combien = carte.lstrip(string.ascii_letters)
    combien = int(combien)
    if direction == "N":    
        l_roi -= combien
    elif direction == "NE":
        l_roi -= combien
        c_roi += combien
    elif direction == "NO":
        l_roi -= combien
        c_roi -= combien
    elif direction == "S":
        l_roi += combien
    elif direction == "SE":
        l_roi += combien
        c_roi += combien
    elif direction == "SO":
        l_roi += combien
        c_roi -= combien                
    elif direction == "O":    
        c_roi -= combien
    elif direction == "E":
        c_roi += combien
    else:
        raise ValueError()
    if couleur == "Rouge":
        plateau[l_roi][c_roi] = "R"
        main_r.remove(carte)
    else:
        plateau[l_roi][c_roi] = "B"
        main_b.remove(carte)
    defausse.append(carte)
    return (plateau, l_roi, c_roi, main_r, main_b, defausse)
    
    
# DÉFINTION DES TERRITOIRES
def territoire(plateau, ligne, colonne, couleur):
    case_N = (-1,0)
    case_O = (0,-1)
    case_S = (+1,0)
    case_E = (0,+1)
    fin = []
    directions = [case_N, case_S, case_O, case_E]
    if couleur == "Rouge":
        if plateau[ligne][colonne] == "R":
            deja_vues = [(ligne, colonne)]
            zone = [(ligne, colonne)]
            while len(deja_vues) != 0:
                for n in deja_vues: 
                    for i in directions: 
                        case_exploree = tuple([a+b for a,b in zip(n, i)]) 
                        if case_exploree not in fin and case_exploree not in deja_vues: 
                            if 0 < case_exploree[0] < len(plateau) and 0 < case_exploree[1] < len(plateau):
                                if plateau[case_exploree[0]][case_exploree[1]] != "R":
                                    fin.append(case_exploree)                                
                                else:
                                    zone.append(case_exploree)
                                    deja_vues.append(case_exploree)                            
                    deja_vues.remove(n)
                    fin.append(n)
            return zone
        else:
            return []
    elif couleur == "Blanc":
        if plateau[ligne][colonne] == "B":
            deja_vues = [(ligne, colonne)]
            zone = [(ligne, colonne)]
            while len(deja_vues) != 0:
                for n in deja_vues: 
                    for i in directions: 
                        case_exploree = tuple([a+b for a,b in zip(n, i)]) 
                        if case_exploree not in fin and case_exploree not in deja_vues: 
                            if 0 < case_exploree[0] < len(plateau) and 0 < case_exploree[1] < len(plateau):
                                if plateau[case_exploree[0]][case_exploree[1]] != "B":
                                    fin.append(case_exploree)                                
                                else:
                                    zone.append(case_exploree)
                                    deja_vues.append(case_exploree)                            
                    deja_vues.remove(n)
                    fin.append(n)
            return zone
        else:
            return []
        
# CALCUL DES SCORES
def score(plateau , couleur):
    rows = 9
    cols = 9
    score = 0
    fin = []
    for l in range(0, rows):
            for c in range(0, cols):
                    if plateau[l][c] == "R" and (l,c) not in fin:
                        voisins = len(territoire(plateau , l , c , couleur))
                        score += voisins**2
                        fin.extend(territoire(plateau, l, c, couleur))
                    elif plateau[l][c] == "B" and (l,c) not in fin:
                        voisins = len(territoire(plateau , l , c , couleur))
                        score += voisins**2
                        fin.extend(territoire(plateau , l , c , couleur))
    return score               

def main():
    plateau, l_roi, c_roi, main_r, main_b, pioche, defausse = init_jeu(9,9)
    joueur = "Rouge"
    position_roi = []
    pions_rouges = 26
    pions_blancs = 26
    jeu_termine = False
    while jeu_termine != True:
        afficher_jeu(plateau, l_roi, c_roi, main_r, main_b)
        if joueur == "Rouge":
            main_jouable(plateau, l_roi, c_roi, main_r)
            demande_action("Rouge" , plateau , l_roi , c_roi , main_r)
            if reponse == carte and choix == "OK":
                bouge_le_roi(plateau, l_roi, c_roi, main_r, main_b, defausse, carte, "Rouge")
                pions_rouges -= 1
            elif reponse == "pioche" and choix == "OK":
                carte_piochee = random.choice(pioche)
                main_r.append(carte_piochee)
                pioche.remove(carte_piochee)
            joueur = "Blanc"
            print(bouge_le_roi(plateau, l_roi, c_roi, main_r, main_b, defausse, carte, "Rouge"))
        elif joueur == "Blanc":
            main_jouable(plateau, l_roi, c_roi, main_b)
            demande_action("Blanc", plateau, l_roi, c_roi, main_b)
            if reponse == carte and choix == "OK":
                bouge_le_roi(plateau, l_roi, c_roi, main_r, main_b, defausse, carte, "Blanc")
                pions_blancs -= 1
            elif reponse == "pioche" and choix == "OK":
                carte_piochee = random.choice(pioche)
                main_b.append(carte_piochee)
                pioche.remove(carte_piochee)
            joueur = "Rouge"
            print(bouge_le_roi(plateau, l_roi, c_roi, main_r, main_b, defausse, carte, "Blanc"))
        if "R" or "B" in plateau:
            for ligne in range(0,rows):
                for colonne in range(0,cols):
                    if [ligne,colonne] not in position_roi:
                        if plateau[ligne][colonne] == 'R':
                            l_roi = ligne
                            c_roi = colonne
                            position_roi.append([ligne,colonne])
                        elif plateau[ligne][colonne] == 'B':
                            l_roi = ligne
                            c_roi = colonne
                            position_roi.append([ligne,colonne])
        if pioche == []:
            pioche = defausse
            defausse = []
        if reponse == "passe" in demande_action("Rouge" , plateau , l_roi , c_roi , main_r) and reponse == "passe" in demande_action("Blanc" , plateau , l_roi , c_roi , main_b):
            jeu_termine = True
        elif pions_rouges == 0 or pions_blancs == 0:
            jeu_termine = True 
        else:
            jeu_termine = False
    if jeu_termine == True:
        print(score(plateau, "Rouge"))
        print(score(plateau, "Blanc"))
        if score(plateau, "Rouge") > score(plateau, "Blanc"):
            print("Rouge a gagné la partie")
        elif score(plateau, "Rouge") < score(plateau, "Blanc"):
            print("Blanc a gagné la partie")
        else:
            print("Égalité")
    return 

if __name__ == "__main__":
    main()
    
