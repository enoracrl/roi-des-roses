# TEST ROI DES ROSES

import pytest
roi_des_roses = pytest.importorskip("roi_des_roses")
class Test_init_jeu:
    '''
    Init_jeu : initialisation du plateau de jeu
    '''
    def test_init_jeu(self):
        # à l'initialisation on doit avoir 7 élements
        assert len(roi_des_roses.init_jeu(9,9)) == 7
    def test_plateau(self):
        '''Test variable plateau'''
        # le plateau doit être vide, c'est-à-dire uniquement constitué de 9 lignes x 9 colonnes composés de "." 
        plateau = (roi_des_roses.init_jeu(9,9))[0]
        assert plateau == [[".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."]]
    def test_roi(self):
        '''Test variable roi'''
        # le roi doit être situé au milieu du plateau : c'est-à-dire aux coordonnées plateau[4][4]
        ligne_roi = (roi_des_roses.init_jeu(9,9))[1]
        assert ligne_roi == 4
        colonne_roi = (roi_des_roses.init_jeu(9,9))[2]
        assert colonne_roi == 4
    def test_main(self):
        '''Test variable main joueurs'''
        # comme les mains sont choisis aléatoirement on vérifie juste si elles ont bien 5 cartes
        main_rouge = (roi_des_roses.init_jeu(9,9))[3]
        assert len(main_rouge) == 5
        main_blanche = (roi_des_roses.init_jeu(9,9))[4]
        assert len(main_blanche) == 5
    def test_pioche(self):
        '''Test variable pioche'''
        # comme la pioche est chosit aléatorement, on vérifie juste que la pioche contient 24 cartes - 10 (2x les mains des joueurs)
        pioche = (roi_des_roses.init_jeu(9,9))[5]
        assert len(pioche) == 24 - 2*5
    def test_defausse(self):
        '''Test variable defausse'''
        # la défausse est une liste vide au début du jeu
        defausse = (roi_des_roses.init_jeu(9,9))[6]
        assert defausse == []

class Test_affichage_main:
    '''
    Affichage des mains des joueurs
    '''
    def test_affichage_main_rouge(self):
        main_rouge = roi_des_roses.afficher_main("Rouge", ["SE2", "SO2", "NE3"])
        if main_rouge == "Rouge : SE2 SO2 NE3":
            print("la main du joueur rouge est OK")
        else:
            print("la main du joueur rouge est KO")
    def test_affichage_main_blanc(self):    
        main_blanche = roi_des_roses.afficher_main("Blanc", ["S1", "SO1", "NO3", "O2", "NE1"])
        if main_blanche == "Blanc : S1 SO1 NO3 O2 NE1":
            print("la main du joueur blanc est OK")
        else:
            print("la main du joueur blanc est KO")
    
class Test_affichage_jeu:
    '''
    Affichage complet en début de partie
    '''
    def test_affichage_jeu_debut_partie(self):
        plateau_debut = roi_des_roses.afficher_jeu(plateau = [[".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."]], l_roi = 4 , c_roi = 4, main_r = ["SE2", "SO2", "N3", "S2", "NE3"], main_b = ["S1", "E1","SO1","NO3","O2","N3"])
        if plateau_debut == "Rouge : SE2 SO2 N3 S2 NE3 \nBlanc : S1 E1 SO1 NO3 O2 N3\n----------------------------------------------\n|    |    |    |    |    |    |    |    |    |\n----------------------------------------------\n|    |    |    |    |    |    |    |    |    |\n----------------------------------------------\n|    |    |    |    |    |    |    |    |    |\n----------------------------------------------\n|    |    |    |    |    |    |    |    |    |\n----------------------------------------------\n|    |    |    |    | X  |    |    |    |    |\n----------------------------------------------\n|    |    |    |    |    |    |    |    |    |\n----------------------------------------------\n|    |    |    |    |    |    |    |    |    |\n----------------------------------------------\n|    |    |    |    |    |    |    |    |    |\n----------------------------------------------\n|    |    |    |    |    |    |    |    |    |\n----------------------------------------------":
            print("Le plateau au début du jeu est OK")
        else:
            print("Le plateau au début du jeu est KO")
    def test_affichage_jeu_milieu_partie(self):    
        plateau_milieu = roi_des_roses.afficher_jeu(plateau = [[".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".","B",".","R",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".","R",".",".",".",".",".","."], ["R","B","B",".","R","B",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."]], l_roi = 6, c_roi = 1, main_r = ["SE2", "SO2", "NE3"], main_b = ["S1", "NO3"])
        if plateau_milieu == "Rouge : SE2 SO2 NE3 \nBlanc : S1 NO3\n----------------------------------------------\n|    |    |    |    |    |    |    |    |    |\n----------------------------------------------\n|    |    |    |    |    |    |    |    |    |\n----------------------------------------------\n|    |    |    |    |    |    |    |    |    |\n----------------------------------------------\n|    |    |    |  B |    |    |  R |    |    |\n----------------------------------------------\n|    |    |    |    |    |    |    |    |    |\n----------------------------------------------\n|    |    |  R |    |    |    |    |    |    |\n----------------------------------------------\n|  R | XB |  B |    |  R |  B |    |    |    |\n----------------------------------------------\n|    |    |    |    |    |    |    |    |    |\n----------------------------------------------\n|    |    |    |    |    |    |    |    |    |\n----------------------------------------------":
           print("Le plateau en milieu de jeu est OK")
        else:
            print("Le plateau en milieu de jeu est KO") 

class Test_mouvement_du_roi:
    '''
    On teste les mouvements possibles du roi en fonction de sa position
    '''
    # mouvement impossible car un pion occupe déjà cette case
    def test_mouvement_roi(self):
        assert roi_des_roses.mouvement_possible(plateau = [[".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".","B",".","R",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".","R",".",".",".",".",".","."], ["R","B","B",".","R","B",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."]], l_roi = 6, c_roi = 1 , carte = "O1") == False
        # mouvement impossible car on sortirai du plateau
        assert roi_des_roses.mouvement_possible(plateau = [[".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".","B",".","R",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".","R",".",".",".",".",".","."], ["R","B","B",".","R","B",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."]], l_roi = 6, c_roi = 1 , carte = "S3") == False
        # mouvement possible car pas de pion sur la case et on reste sur le plateau
        assert roi_des_roses.mouvement_possible(plateau = [[".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".","B",".","R",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".","R",".",".",".",".",".","."], ["R","B","B",".","R","B",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."]], l_roi = 6, c_roi = 1 , carte = "N2") == True

class Test_main_jouable:
    '''
    On affiche les cartes jouables de la main du joueur en cours
    '''
    def test_main_jouable(self):
        # main_rouge
        assert roi_des_roses.main_jouable(plateau = [[".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".","B",".","R",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".","R",".",".",".",".",".","."], ["R","B","B",".","R","B",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."]], l_roi = 6, c_roi = 1 , main = ["SE2", "SO2", "NE3"]) == ["SE2","NE3"]
        # main_blanche
        assert roi_des_roses.main_jouable(plateau = [[".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".","B",".","R",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".","R",".",".",".",".",".","."], ["R","B","B",".","R","B",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."]], l_roi = 6, c_roi = 1 , main = ["S1", "NO3"]) == ["S1"]
        # si aucune carte jouables
        assert roi_des_roses.main_jouable(plateau = [[".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".","B",".","R",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".","R",".",".",".",".",".","."], ["R","B","B",".","R","B",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."]], l_roi = 6, c_roi = 1 , main = ["S3", "NO3", "O1"]) == []

class Test_demande_action:
    '''
    On teste les mouvements possibles du roi en fonction de sa position
    '''
    # mouvement impossible car un pion occupe déjà cette case
    def test_demande_action(self):  
        demande_action_rouge = roi_des_roses.demande_action(couleur = "Rouge" , plateau = [[".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".","B",".","R",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".","R",".",".",".",".",".","."], ["R","B","B",".","R","B",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."]] , l_roi = 6, c_roi = 1 , main = ["SE2", "SO2", "NE3"])
        if demande_action_rouge == "Vous etes le joueur Rouge , que souhaitez-vous faire ?" and input():
            if input() == "passe" and demande_action_rouge == "Action impossible, que souhaitez-vous faire ?":
                print("demande action est OK")
            elif input() == "pioche" and demande_action_rouge != "Action impossible, que souhaitez-vous faire ?":
                print("demande action est OK")
            elif input() == "SO2" or "NE3" and demande_action_rouge != "Action impossible, que souhaitez-vous faire ?":
                print("demande action est OK")
            elif input() == "SE2" and demande_action_rouge == "Action impossible, que souhaitez-vous faire ?":
                print("demande action est OK")
            else:
                print("demande action est KO")
        else:
            print("demande action est KO")
        demande_action_blanc = roi_des_roses.demande_action(couleur = "Blanc" , plateau = [[".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".","B",".","R",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".","R",".",".",".",".",".","."], ["R","B","B",".","R","B",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."]] , l_roi = 6, c_roi = 1 , main = ["S1", "NO3"])
        if demande_action_blanc == "Vous etes le joueur Blanc , que souhaitez-vous faire ?" and input():
                if input() == "passe" and demande_action_blanc == "Action impossible, que souhaitez-vous faire ?":
                    print("demande action est OK")
                elif input() == "pioche" and demande_action_blanc != "Action impossible, que souhaitez-vous faire ?":
                    print("demande action est OK")
                elif input() == "S1" and demande_action_blanc != "Action impossible, que souhaitez-vous faire ?":
                    print("demande action est OK")
                elif input() == "NO3" and demande_action_blanc == "Action impossible, que souhaitez-vous faire ?":
                    print("demande action est OK")
                else:
                    print("demande action est KO")
        else:
            print("demande action est KO")

class Test_bouge_le_roi:
    def test_bouge_le_roi(self):
        assert len(roi_des_roses.bouge_le_roi(plateau = [[".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".","B",".","R",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".","R",".",".",".",".",".","."], ["R","B","B",".","R","B",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."]] , l_roi = 6, c_roi = 1 , main_r = ["SE2", "SO2", "NE3"], main_b = ["S1", "NO3"], defausse = [], carte = "NE3", couleur ="Rouge")) == 6
        assert roi_des_roses.bouge_le_roi(plateau = [[".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".","B",".","R",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".","R",".",".",".",".",".","."], ["R","B","B",".","R","B",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."]] , l_roi = 6, c_roi = 1 , main_r = ["SE2", "SO2", "NE3"], main_b = ["S1", "NO3"], defausse = [], carte = "NE3", couleur ="Rouge") == ([[".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".","B","R","R",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".","R",".",".",".",".",".","."], ["R","B","B",".","R","B",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."]], 3, 4, ["SE2", "SO2"], ["S1", "NO3"], ["NE3"])
        assert roi_des_roses.bouge_le_roi(plateau = [[".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".","B","R","R",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".","R",".",".",".",".",".","."], ["R","B","B",".","R","B",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."]] , l_roi = 3, c_roi = 4 , main_r = ["SE2", "SO2"], main_b = ["S1", "NO3"], defausse = ["NE3"], carte = "S1", couleur ="Blanc") == ([[".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".","B","R","R",".",".","."], [".",".",".",".","B",".",".",".","."], [".",".","R",".",".",".",".",".","."], ["R","B","B",".","R","B",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."]], 4, 4, ["SE2", "SO2"], ["NO3"], ["NE3", "S1"])
class Test_territoire:
    def test_territoire(self):
        assert roi_des_roses.territoire(plateau = [[".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".","B","R","R","R",".","."], [".",".",".",".",".",".",".",".","."], [".",".","R",".",".",".",".",".","."], ["R","B","B",".","R","B","R",".","."], [".",".","B","B","B",".",".",".","."], [".",".","B",".",".",".",".",".","."]] , ligne = 3 , colonne = 4 , couleur = "Rouge") == [(3, 4), (3, 5), (3, 6)]
        assert roi_des_roses.territoire(plateau = [[".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".","B","R","R","R",".","."], [".",".",".",".",".",".",".",".","."], [".",".","R",".",".",".",".",".","."], ["R","B","B",".","R","B","R",".","."], [".",".","B","B","B",".",".",".","."], [".",".","B",".",".",".",".",".","."]] , ligne = 7 , colonne = 2 , couleur = "Rouge") == []
        assert roi_des_roses.territoire(plateau = [[".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".","B","R","R","R",".","."], [".",".",".",".",".",".",".",".","."], [".",".","R",".",".",".",".",".","."], ["R","B","B",".","R","B","R",".","."], [".",".","B","B","B",".",".",".","."], [".",".","B",".",".",".",".",".","."]] , ligne = 3 , colonne = 3 , couleur = "Blanc") == [(3, 3)]
        assert roi_des_roses.territoire(plateau = [[".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".","B","R","R","R",".","."], [".",".",".",".",".",".",".",".","."], [".",".","R",".",".",".",".",".","."], ["R","B","B",".","R","B","R",".","."], [".",".","B","B","B",".",".",".","."], [".",".","B",".",".",".",".",".","."]] , ligne = 7 , colonne = 2 , couleur = "Blanc") == [(7, 2), (6, 2), (8, 2), (7, 3), (6, 1), (7, 4)]

class Test_scores:
    def test_scores(self):
        assert roi_des_roses.score(plateau = [[".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".","B","R","R","R",".","."], [".",".",".",".",".",".",".",".","."], [".",".","R",".",".",".",".",".","."], ["R","B","B",".","R","B","R",".","."], [".",".","B","B","B",".",".",".","."], [".",".","B",".",".",".",".",".","."]], couleur = "Rouge") == 13
        assert roi_des_roses.score(plateau = [[".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".",".",".",".",".",".","."], [".",".",".","B","R","R","R",".","."], [".",".",".",".",".",".",".",".","."], [".",".","R",".",".",".",".",".","."], ["R","B","B",".","R","B","R",".","."], [".",".","B","B","B",".",".",".","."], [".",".","B",".",".",".",".",".","."]], couleur = "Blanc") == 38

ia = pytest.importorskip("ia")
