# MODELISATION

import random
import string

def init_jeu(nb_lignes, nb_colonnes):
    # LE PLATEAU
    global cols
    cols = nb_colonnes
    global rows
    rows = nb_lignes
    plateau = []
    for _ in range(nb_lignes):
        plateau.append(["."]*rows)
    
    # LE ROI
    c_roi = nb_colonnes // 2
    l_roi = nb_lignes // 2
    
    # LA PIOCHE
    directions = ["N", "NE", "E", "SE", "S", "SO", "O", "NO"]
    nombres = ["1", "2", "3"]
    pioche = []
    n = 0
    while n != len(directions):
        k = 0
        while k != len(nombres): 
            pioche.append(directions[n] + nombres[k])
            k += 1
        n += 1
    random.shuffle(pioche)

    # LA MAIN DES JOUEURS
    # main du joueur rouge
    main_r = random.sample(pioche,5)
    pioche = set(pioche) - set(main_r)
    setp = set(pioche)
    setmr = set(main_r)
    pioche = list(setp - setmr)

    # main du joueur blanc
    main_b = random.sample(pioche,5)
    pioche = set(pioche) - set(main_b)
    setp = set(pioche)
    setmb = set(main_b)
    pioche = list(setp - setmb)

    # LA DEFAUSSE
    global defausse
    defausse = []
    return (plateau, l_roi, c_roi, main_r, main_b, pioche, defausse)

# AFFICHAGE DE LA MAIN D'UN JOUEUR
def afficher_main(couleur , main):
    if len(main) == 5:
        print(couleur, ":", main[0], main[1], main[2], main[3], main[4])    
    elif len(main) == 4:
        print(couleur, ":", main[0], main[1], main[2], main[3])
    elif len(main) == 3:
        print(couleur, ":", main[0], main[1], main[2])
    elif len(main) == 2:
        print(couleur, ":", main[0], main[1])
    elif len(main) == 1:
        print(couleur, ":", main[0])
    else:
        print(couleur, ":")
    return main

# AFFICHAGE DU PLATEAU DE JEU 
def afficher_jeu(plateau , l_roi , c_roi , main_r , main_b):
    afficher_main("Rouge", main_r)
    afficher_main("Blanc", main_b)
    ligne = "-" * 5 * cols
    global roi, cases
    case_vide = "|" + " " + " " + " " 
    pion_rouge = "|" + " " + " " + "R"
    pion_blanc = "|" + " " + " " + "B"
    roi = "|" + " " + "X" + " "
    pion_rouge_et_roi = "|" + " " + "X" + "R"
    pion_blanc_et_roi = "|" + " " + "X" + "B"
    for l in range(0, rows):
        print(ligne)
        cases = []
        for c in range(0, cols):
            if l == l_roi and c == c_roi:
                if plateau[l][c] == "R":
                    cases.append(pion_rouge_et_roi)
                elif plateau[l][c] == "B":
                    cases.append(pion_blanc_et_roi)
                else:
                    cases.append(roi)
            else:
                if plateau[l][c] == "R":
                    cases.append(pion_rouge)
                elif plateau[l][c] == "B":                
                    cases.append(pion_blanc)
                elif plateau[l][c] == ".":
                    cases.append(case_vide)       
            cases2 = str(cases)
            cases2 = cases2.replace("'", "")
            cases2 = cases2.replace(",", "")
            cases2 = cases2.replace("[", "")
            cases2 = cases2.replace("]", "")
        print(cases2, sep = "", end = " |\n")
    print(ligne)

# DÉFINITION DES MOUVEMENTS POSSIBLES DU ROI
def mouvement_possible(plateau , l_roi , c_roi , carte):
    direction = carte.rstrip(string.digits)
    combien = carte.lstrip(string.ascii_letters)
    combien = int(combien)
    l_roi_possible = l_roi
    c_roi_possible = c_roi
    if direction == "N":    
        l_roi_possible -= combien
    elif direction == "NE":
        l_roi_possible -= combien
        c_roi_possible += combien
    elif direction == "NO":
        l_roi_possible -= combien
        c_roi_possible -= combien
    elif direction == "S":
        l_roi_possible += combien
    elif direction == "SE":
        l_roi_possible += combien
        c_roi_possible += combien
    elif direction == "SO":
        l_roi_possible += combien
        c_roi_possible -= combien                
    elif direction == "O":    
        c_roi_possible -= combien
    elif direction == "E":
        c_roi_possible += combien
    else:
        raise ValueError()
    if (l_roi_possible < 0 or l_roi_possible > 8 or c_roi_possible < 0 or c_roi_possible > 8):
        return False
    elif (plateau[l_roi_possible][c_roi_possible] != "."):
        return False
    else: 
        return True

# DÉFINTION DES CARTES JOUABLES DANS LA MAIN DU JOUEUR    
def main_jouable(plateau , l_roi , c_roi , main):
    global cartes_jouables
    cartes_jouables = []
    for carte in main:
        if mouvement_possible(plateau , l_roi , c_roi , carte) == True:
            cartes_jouables.append(carte)
    if cartes_jouables == []:
        return []
    return cartes_jouables

# DEMANDER AU JOUEUR L'ACTION À EFFECTUER
def demande_action(couleur , plateau , l_roi , c_roi , main):
    global choix, reponse, carte
    print("Vous etes le joueur", couleur, ", que souhaitez-vous faire ? ")
    choix = "KO"
    while choix != "OK":
        reponse = input()
        if reponse == "pioche":
            if len(main) >= 5:
                print("Action impossible, que souhaitez-vous faire ? ")
                choix = "KO"
            else :
                choix = "OK"
        elif reponse == "passe":
            if len(main) < 5:
                print("Action impossible, que souhaitez-vous faire ? ")
                choix = "KO"
            elif cartes_jouables != []:
                print("Action impossible, que souhaitez-vous faire ? ")
                choix = "KO"   
            else:
                choix = "OK"
        elif reponse in main :
            if len(main) == 0:
                print("Action impossible, que souhaitez-vous faire ? ")
                choix = "KO"
            elif cartes_jouables == []:
                print("Action impossible, que souhaitez-vous faire ? ")
                choix = "KO"
            else:
                carte = reponse
                if mouvement_possible(plateau , l_roi , c_roi , carte) == False:
                    print("Action impossible, que souhaitez-vous faire ? ")
                    choix = "KO"
                else:
                    if carte not in cartes_jouables:
                        print("Action impossible, que souhaitez-vous faire ? ")
                        choix = "KO"
                    else:
                        choix = "OK"
                        return carte
        else:
            print("Action impossible, que souhaitez-vous faire ? ")
    return reponse, choix

def bouge_le_roi(plateau, l_roi, c_roi, main_r, main_b, defausse , carte , couleur):
    direction = carte.rstrip(string.digits)
    combien = carte.lstrip(string.ascii_letters)
    combien = int(combien)
    if direction == "N":    
        l_roi -= combien
    elif direction == "NE":
        l_roi -= combien
        c_roi += combien
    elif direction == "NO":
        l_roi -= combien
        c_roi -= combien
    elif direction == "S":
        l_roi += combien
    elif direction == "SE":
        l_roi += combien
        c_roi += combien
    elif direction == "SO":
        l_roi += combien
        c_roi -= combien                
    elif direction == "O":    
        c_roi -= combien
    elif direction == "E":
        c_roi += combien
    else:
        raise ValueError()
    if couleur == "Rouge":
        plateau[l_roi][c_roi] = "R"
        main_r.remove(carte)
    elif couleur == "Blanc":
        plateau[l_roi][c_roi] = "B"
        main_b.remove(carte)
    defausse.append(carte)
    return (plateau, l_roi, c_roi, main_r, main_b, defausse)
    
# DÉFINTION DES TERRITOIRES
def territoire(plateau, ligne, colonne, couleur):
    case_N = (-1,0)
    case_O = (0,-1)
    case_S = (+1,0)
    case_E = (0,+1)
    fin = []
    directions = [case_N, case_S, case_O, case_E]
    if couleur == "Rouge":
        if plateau[ligne][colonne] == "R":
            deja_vues = [(ligne, colonne)]
            zone = [(ligne, colonne)]
            while len(deja_vues) != 0:
                for n in deja_vues: 
                    for i in directions: 
                        case_exploree = tuple([a+b for a,b in zip(n, i)]) 
                        if case_exploree not in fin and case_exploree not in deja_vues: 
                            if 0 < case_exploree[0] < len(plateau) and 0 < case_exploree[1] < len(plateau):
                                if plateau[case_exploree[0]][case_exploree[1]] != "R":
                                    fin.append(case_exploree)                                
                                else:
                                    zone.append(case_exploree)
                                    deja_vues.append(case_exploree)                            
                    deja_vues.remove(n)
                    fin.append(n)
            return zone
        else:
            return []
    elif couleur == "Blanc":
        if plateau[ligne][colonne] == "B":
            deja_vues = [(ligne, colonne)]
            zone = [(ligne, colonne)]
            while len(deja_vues) != 0:
                for n in deja_vues: 
                    for i in directions: 
                        case_exploree = tuple([a+b for a,b in zip(n, i)]) 
                        if case_exploree not in fin and case_exploree not in deja_vues: 
                            if 0 < case_exploree[0] < len(plateau) and 0 < case_exploree[1] < len(plateau):
                                if plateau[case_exploree[0]][case_exploree[1]] != "B":
                                    fin.append(case_exploree)                                
                                else:
                                    zone.append(case_exploree)
                                    deja_vues.append(case_exploree)                            
                    deja_vues.remove(n)
                    fin.append(n)
            return zone
        else:
            return []
        
# CALCUL DES SCORES
def score(plateau , couleur):
    score = 0
    fin = []
    for l in range(0, rows):
            for c in range(0, cols):
                    if plateau[l][c] == "R" and (l,c) not in fin:
                        voisins = len(territoire(plateau , l , c , couleur))
                        score += voisins**2
                        fin.extend(territoire(plateau, l, c, couleur))
                    elif plateau[l][c] == "B" and (l,c) not in fin:
                        voisins = len(territoire(plateau , l , c , couleur))
                        score += voisins**2
                        fin.extend(territoire(plateau , l , c , couleur))
    return score               

def name():
    # c'est le nom de mon beau gosse de chat : 0 originalité
    return "Billy"

def play(plateau , l_roi , c_roi , main_r , main_b , couleur):
    global carte_ia
    main = main_b
    pion = couleur[0]
    if pion == "R":
        pion_adverse = "B"
    else:
        pion_adverse = "R"
    if couleur == "Rouge":
        main = main_r
    #if len(main) < 5: 
    #    return "pioche"
    #else:
    for carte_ia in main:
        if mouvement_possible(plateau , l_roi , c_roi , carte = carte_ia):
            # au premier tour
            if pion not in plateau:
            # on va anticiper la position du roi
            # si le roi est dans la zone nord du plateau
                if l_roi < 4:
                    # si le roi est dans la zone NO du plateau
                    if c_roi < 4 :
                        # on retourne au milieu du plateau si l'on dispose d'une de ces cartes
                        # meilleur choix
                        if carte_ia == "SE1" or "SE2" or "SE3":
                            return carte_ia
                        # second choix
                        elif carte_ia == "O1" or "O2" or "O3" or "S1" or "S2" or "S3":
                            return carte_ia
                        # si aucune de ces cartes, on laisse le hasard décider, pas le choix 
                    # si le roi est dans la zone NE du plateau
                    elif c_roi > 4 :
                        if carte_ia == "SO1" or "SO2" or "SO3":
                            return carte_ia
                        elif carte_ia == "E1" or "E2" or "E3" or "S1" or "S2" or "S3":
                            return carte_ia
                    # si le roi se trouve sur la moitié nord de la colonne du milieu du plateau (c_roi == 4)
                    else :
                        if carte_ia == "S1" or "S2" or "S3":
                            return carte_ia
                        elif carte_ia == "SE1" or "SE2" or "SE3" or "SO1" or "SO2" or "SO3":
                            return carte_ia
                # si le roi est dans la zone sud du plateau
                elif l_roi > 4:
                    # si le roi est dans la zone SO du plateau
                    if c_roi < 4 :
                        if carte_ia == "NE1" or "NE2" or "NE3":
                            return carte_ia
                        elif carte_ia == "O1" or "O2" or "O3" or "N1" or "N2" or "N3":
                            return carte_ia
                    # si le roi est dans la zone SE du plateau
                    elif c_roi > 4 :
                        if carte_ia == "NO1" or "NO2" or "NO3":
                            return carte_ia
                        elif carte_ia == "E1" or "E2" or "E3" or "S1" or "S2" or "S3":
                            return carte_ia
                    # si le roi se trouve sur la moitié sud de la colonne du milieu du plateau (c_roi == 4)   
                    else :
                        # premier choix
                        if carte_ia == "N1" or "N2" or "N3":
                            return carte_ia
                        # second choix si on a pas les cartes du dessus dans notre main
                        elif carte_ia == "NE1" or "NE2" or "NE3" or "NO1" or "NO2" or "NO3":
                            return carte_ia
                # si le roi se trouve sur ligne du milieu du plateau (l_roi == 4)
                else :
                    # si le roi se situe dans la partie ouest de la ligne du milieu
                    if c_roi < 4 :
                        # premier choix
                        if carte_ia == "E1" or "E2" or "E3":
                            return carte_ia
                        # deuxieme choix
                        elif carte_ia == "SE1" or "SE2" or "SE3" or "NE1" or "NE2" or "NE3":
                            return carte_ia
                    elif c_roi > 4 :
                        if carte_ia == "O1" or "O2" or "O3":
                            return carte_ia
                    # le roi se situe pile au milieu du plateau : c'est à l'IA de commencer 
                    # on décide de déplacer le roi d'une case au mieux si on dispose d'une de ces cartes ci-dessous (8 chances/24 soit 1 chance/3 d'avoir la carte)
                    # cela permet de commencer notre zone au milieu du plateau pour l'étendre ensuite
                    else:
                        if carte_ia == "O1" or "E1" or "S1" or "N1" or "NE1" or "NO1" or "SE1" or "SO1":
                            return carte_ia
            else:   
            # si dans un périmètre de 3x3 cases autour du roi il y a un pion de notre couleur, on essaye de s'en rapprocher (au mieux juste à côté)
            # # on definit les limites de notre perimetre
                l_roiperi_min = l_roi - 3
                l_roiperi_max = l_roi + 3
                c_roiperi_min = c_roi - 3
                c_roiperi_max = c_roi + 3
                if l_roiperi_max > 8 :
                    l_roiperi_max = 8
                if l_roiperi_min < 0:
                    l_roiperi_min = 0
                if c_roiperi_max > 8 :
                    c_roiperi_max = 8
                if c_roiperi_min < 0:
                    c_roiperi_min = 0
                perimetre_roi = []       
                for i in range(0, 9):
                    perimetre_roi.append([])
                    for j in range(c_roiperi_min,c_roiperi_max+1):
                        if c_roiperi_min <= j <= c_roiperi_max:
                            perimetre_roi[i].append(plateau[i][j])
                perimetre_roi = perimetre_roi[l_roiperi_min:l_roiperi_max+1]
                if pion in perimetre_roi:
                    for i in range(0,len(perimetre_roi)):
                        for j in range(0,len(perimetre_roi)):
                            if perimetre_roi[i][j] == pion:
                                if bouge_le_roi(plateau, l_roi, main_r, main_b, defausse, carte_ia, couleur) == (plateau, i, j-1, main_r, main_b, defausse):
                                    return carte_ia
                                elif bouge_le_roi(plateau, l_roi, main_r, main_b, defausse, carte_ia, couleur) == (plateau, i, j+1, main_r, main_b, defausse):
                                    return carte_ia
                                elif bouge_le_roi(plateau, l_roi, main_r, main_b, defausse, carte_ia, couleur) == (plateau, i+1, j, main_r, main_b, defausse):
                                    return carte_ia
                                elif bouge_le_roi(plateau, l_roi, main_r, main_b, defausse, carte_ia, couleur) == (plateau, i-1, j, main_r, main_b, defausse):
                                    return carte_ia
                                elif len(main) < 5: 
                                    return "pioche"
                                else:
                                    return carte_ia
                else:
                    # on essaye en premier lieu d'empêcher le joueur adverse d'agrandir son eventuel zone 
                    # comme c'est le cas au morpion ou au puissance 4, on place un de nos pions à l'interstice de deux de ses pions pour minimiser sa zone à lui
                    if pion_adverse in perimetre_roi:
                        for i in range(0,len(perimetre_roi)):
                            for j in range(0,len(perimetre_roi)):
                                if perimetre_roi[i][j] == pion_adverse:
                                    # si il y a un pion adverse à deux cases à gauche de ce pion
                                    if perimetre_roi[i][j-2] == pion_adverse:
                                        if bouge_le_roi(plateau, l_roi, main_r, main_b, defausse, carte_ia, couleur) == (plateau, i, j-1, main_r, main_b, defausse):
                                            return carte_ia
                                    # si il y a un pion adverse à deux cases à droite de ce pion
                                    elif perimetre_roi[i][j+2] == pion_adverse:
                                            if bouge_le_roi(plateau, l_roi, main_r, main_b, defausse, carte_ia, couleur) == (plateau, i, j+1, main_r, main_b, defausse):
                                                return carte_ia
                                    # si il y a un pion adverse à deux cases en haut de ce pion
                                    elif perimetre_roi[i-2][j] == pion_adverse:
                                        if bouge_le_roi(plateau, l_roi, main_r, main_b, defausse, carte_ia, couleur) == (plateau, i-1, j, main_r, main_b, defausse):
                                            return carte_ia
                                    # si il y a un pion adverse à deux cases en bas de ce pion
                                    elif perimetre_roi[i+2][j] == pion_adverse:
                                        if bouge_le_roi(plateau, l_roi, main_r, main_b, defausse, carte_ia, couleur) == (plateau, i+1, j, main_r, main_b, defausse):
                                            return carte_ia
                                    else:
                                        if len(main) < 5: 
                                            return "pioche"
                    # on joue une carte dans la direction la plus éloignée des pions du joueur adverse pour que ça soit plus compliqué pour le joueur adverse ensuite
                    else:
                        for i in range(0,len(plateau)):
                            for j in range(0,len(plateau)):
                                if plateau[i][j] == pion_adverse:
                                    if carte_ia == "SE3" or "S3" or "O3" or "E3" or "N3" or "NO3":
                                       return carte_ia
    if len(main) < 5: 
        return "pioche"
    else:
        return "passe"

def name_ia_prof():
    return "Tata Suzanne"

def play_prof(plateau , l_roi , c_roi , main_r , main_b , couleur):
    global carte_prof
    main = main_b
    if couleur == "Rouge":
        main = main_r 
    if len(main) < 5: 
        return "pioche"
    else:
        for carte_prof in main:
            if mouvement_possible(plateau , l_roi , c_roi , carte_prof): 
                return carte_prof
    return "passe"

def main():
    plateau, l_roi, c_roi, main_r, main_b, pioche, defausse = init_jeu(9,9)
    joueur = "Rouge"
    position_roi = []
    pions_rouges = 26
    pions_blancs = 26
    #ia = "Blanc"
    jeu_termine = False
    while jeu_termine != True:
        afficher_jeu(plateau, l_roi, c_roi, main_r, main_b)
        if joueur == "Rouge":
            print("C'est au tour de", name_ia_prof())
            main_jouable(plateau, l_roi, c_roi, main_r)
            reponse = play_prof(plateau, l_roi, c_roi, main_r, main_b, "Rouge")
            reponse
            print(reponse)
            if reponse == "pioche":
                carte_piochee = random.choice(pioche)
                main_r.append(carte_piochee)
                pioche.remove(carte_piochee)
            elif reponse == "passe":
                pass
            else:
                bouge_le_roi(plateau, l_roi, c_roi, main_r, main_b, defausse, carte_prof, "Rouge")
                pions_rouges -= 1
            joueur = "Blanc"
        elif joueur == "Blanc":
            print("C'est au tour de", name())
            main_jouable(plateau, l_roi, c_roi, main_b)
            reponse2 = play(plateau, l_roi, c_roi, main_r, main_b, "Blanc")
            reponse2
            print(reponse2)
            if reponse2 == "pioche":
                carte_piochee = random.choice(pioche)
                main_b.append(carte_piochee)
                pioche.remove(carte_piochee)
            elif reponse2 == "passe":
                pass
            else:
                bouge_le_roi(plateau, l_roi, c_roi, main_r, main_b, defausse, carte_ia, "Blanc")
                pions_blancs -= 1
            joueur = "Rouge"
        if "R" or "B" in plateau:
            for ligne in range(0,rows):
                for colonne in range(0,cols):
                    if [ligne,colonne] not in position_roi:
                        if plateau[ligne][colonne] == 'R':
                            l_roi = ligne
                            c_roi = colonne
                            position_roi.append([ligne,colonne])
                        elif plateau[ligne][colonne] == 'B':
                            l_roi = ligne
                            c_roi = colonne
                            position_roi.append([ligne,colonne])
        if pioche == []:
            pioche = defausse
            defausse = []
        if reponse == "passe" and reponse2 == "passe" :
            jeu_termine = True
        elif pions_rouges == 0 or pions_blancs == 0:
            jeu_termine = True 
        else:
            jeu_termine = False
    if jeu_termine == True:
        print(score(plateau, "Rouge"))
        print(score(plateau, "Blanc"))
        if score(plateau, "Rouge") > score(plateau, "Blanc"):
            print("Rouge a gagné la partie")
        elif score(plateau, "Rouge") < score(plateau, "Blanc"):
            print("Blanc a gagné la partie")
        else:
            print("Égalité")
    return 

if __name__ == "__main__":
    main()
    
